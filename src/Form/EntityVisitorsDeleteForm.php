<?php

namespace Drupal\entity_visitors\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Entity visitors entities.
 *
 * @ingroup entity_visitors
 */
class EntityVisitorsDeleteForm extends ContentEntityDeleteForm {


}
